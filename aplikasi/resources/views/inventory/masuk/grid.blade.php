@extends('layouts.inventorymasuk')
@section('title','Inventory Barang Masuk')
@section('masuk')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
<div class="container">
	<h2>Inventory Barang Masuk</h2>
	<br>
	<div class="row">
		<div class="col-md-9">
			<a href="{{ url('inventory.masuk.form') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
				<span>
					<i class="la la-plus"></i>
					<span>Tambah Data</span>
				</span>
			</a>
		</div>
<!-- 		<div class="col">
			<form action="#" method="get" class="form-inline">
				<input class="search-box mr-sm-2 col-md-9 px-2 form-control" type="text" placeholder="Search.." name="cari">
				<button class="btn btn-sm btn-primary form-control" type="submit"><i class="fa fa-search"></i></button>
			</form>
		</div> -->
	</div>
	<br>
	<table class="table table-hover table-sm" width="100%">
		<tr>
			<th width="20px">No.</th>
			<th width="40%" class="text-center">Tanggal</th>
			<th width="40%" class="text-center">Keterangan</th>
			<th width="40%" class="text-center">Action</th>
		</tr>

		
		<tr>
			<td><b>1</b></td>
			<td class="text-center">fendianwar</td>
			<td class="text-center">trenggalek</td>
			<td class="text-center">
				<form action="#" method="post">
					<a class="btn btn-sm btn-success" href="#">Show</a>
					<a class="btn btn-sm btn-warning" href="#">edit</a>
					
					<button type="submit" class="btn btn-sm btn-danger">Delete</button>
				</form>
			</td>
		</tr>
		<tr>
			<td><b>2</b></td>
			<td class="text-center">fendianwar</td>
			<td class="text-center">trenggalek</td>
			<td class="text-center">
				<form action="#" method="post">
					<a class="btn btn-sm btn-success" href="#">Show</a>
					<a class="btn btn-sm btn-warning" href="#">edit</a>
					
					<button type="submit" class="btn btn-sm btn-danger">Delete</button>
				</form>
			</td>
		</tr>
		<tr>
			<td><b>3</b></td>
			<td class="text-center">fendianwar</td>
			<td class="text-center">trenggalek</td>
			<td class="text-center">
				<form action="#" method="post">
					<a class="btn btn-sm btn-success" href="#">Show</a>
					<a class="btn btn-sm btn-warning" href="#">edit</a>
					
					<button type="submit" class="btn btn-sm btn-danger">Delete</button>
				</form>
			</td>
		</tr>
		<tr>
			<td><b>4</b></td>
			<td class="text-center">fendianwar</td>
			<td class="text-center">trenggalek</td>
			<td class="text-center">
				<form action="#" method="post">
					<a class="btn btn-sm btn-success" href="#">Show</a>
					<a class="btn btn-sm btn-warning" href="#">edit</a>
					
					<button type="submit" class="btn btn-sm btn-danger">Delete</button>
				</form>
			</td>
		</tr>
		<tr>
			<td><b>5</b></td>
			<td class="text-center">fendianwar</td>
			<td class="text-center">trenggalek</td>
			<td class="text-center">
				<form action="#" method="post">
					<a class="btn btn-sm btn-success" href="#">Show</a>
					<a class="btn btn-sm btn-warning" href="#">edit</a>
					
					<button type="submit" class="btn btn-sm btn-danger">Delete</button>
				</form>
			</td>
		</tr>

		
	</table>
	<br>
</div>
</div>
@endsection