@extends('layouts.masteruser')
@section('title','Data User')
@section('masteruser')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 style="text-align: center;">Data Master User</h1>
			<a href="{{url('master.user.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air float-right">
				<span>
					<i class="la la-plus"></i>
					<span>Tambah User</span>
				</span>
			</a>

			<form action="/search" method="get" class="form-inline my-2 my-lg-0" autocomplete="off">
				<label class="text-primary">Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="m_table_1"></label>
	    	</form>
		</div>
	</div>
	<table class="table table-bordered table-hover">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">No</th>
	      <th scope="col">Username</th>
	      <th scope="col">Email</th>
	      <th scope="col">Role</th>
	      <th scope="col">Status</th>
	      <th scope="col">Aksi</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th scope="row">1</th>
	      <td>Mark</td>
	      <td>Mark@gmail.com</td>
	      <td>User</td>
	      <td>Online</td>
	      <td>
	      <a href="{{url('master.user.edit_user')}}" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
	      <a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-trash"></i></a>
	      </td>
	    </tr>
	    <tr>
	      <th scope="row">2</th>
	      <td>Jacob</td>
	      <td>Jacob@gmail.com</td>
	      <td>Admin</td>
	      <td>Offline</td>
	      <td>
	      <a href="{{url('master.user.edit_user')}}" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
	      <a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-trash"></i></a>
	      </td>
	    </tr>
	    <tr>
	      <th scope="row">3</th> 
	      <td>Larry the Bird</td>
	      <td>Larrythebird@gmail.com</td>
	      <td>User</td>
	      <td>Online</td>
	      <td>
	      <a href="{{url('master.user.edit_user')}}" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
	      <a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-trash"></i></a>
	      </td>
	    </tr>
	  </tbody>
	</table>
</div>
@endsection