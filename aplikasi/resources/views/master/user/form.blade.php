@extends('layouts.masteruser')
@section('title','Form User')
@section('masteruser')
<div class="col-lg-12">
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Form User
					</h3>
				</div>
			</div>
		</div>

		<!--begin::Form-->
		<form class="m-form m-form--label-align-right">
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Username:</label>
						<div class="col-lg-6">
							<input type="text" class="form-control m-input" placeholder="username" value="">
							<span class="m-form__help">Masukkan Username</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Email:</label>
						<div class="col-lg-6">
							<input type="email" class="form-control m-input" placeholder="Email" value="">
							<span class="m-form__help">Masukkan Email Baru</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-2">Role</label>
						<div class="col-lg-6">
							<select class="form-control m-input" name="option">
								<option>Admin</option>
								<option>User</option>
							</select>
							<span class="m-form__help">Pilih Role</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Status:</label>
						<div class="col-lg-6">
							<input type="email" class="form-control m-input" placeholder="Status" value="">
							<span class="m-form__help">Masukkan Status</span>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							<button type="submit" class="btn btn-primary">Tambah</button>
							<a href="{{url('master.user.grid')}}" class="btn btn-secondary">Batal</a>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>
@endsection							