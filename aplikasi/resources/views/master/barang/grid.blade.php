@extends('layouts.masterbarang')
@section('title','Data Barang')
@section('masterbarang')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 style="text-align: center;">Data Master Barang</h1>
			<a href="{{url('master.barang.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air float-right">
				<span>
					<i class="la la-plus"></i>
					<span>Tambah Barang</span>
				</span>
			</a>

			<form action="/search" method="get" class="form-inline my-2 my-lg-0" autocomplete="off">
				<label class="text-primary">Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="m_table_1"></label>
	    	</form>
		</div>
	</div>
	<table class="table table-bordered table-hover">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">No</th>
	      <th scope="col">Nama Barang</th>
	      <th scope="col">Harga</th>
	      <th scope="col">Foto</th>
	      <th scope="col">Aksi</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th scope="row">1</th>
	      <td>Mark</td>
	      <td>5000.000</td>
	      <td>1.jpg</td>
	      <td>
	      <a href="{{url('master.barang.edit_barang')}}" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
	      <a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-trash"></i></a>
	      </td>
	    </tr>
	    <tr>
	      <th scope="row">2</th>
	      <td>Jacob</td>
	      <td>6000.000</td>
	      <td>2.jpg</td>
	      <td>
	      <a href="{{url('master.barang.edit_barang')}}" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
	      <a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-trash"></i></a>
	      </td>
	    </tr>
	    <tr>
	      <th scope="row">3</th> 
	      <td>Larry the Bird</td>
	      <td>7000.000</td>
	      <td>3.jpg</td>
	      <td>
	      <a href="{{url('master.barang.edit_barang')}}" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
	      <a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-trash"></i></a>
	      </td>
	    </tr>
	  </tbody>
	</table>
</div>
@endsection