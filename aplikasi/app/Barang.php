<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    Protected $table ='barang';
    Protected $fillable =['id','nama_barang','harga','foto'];
}
