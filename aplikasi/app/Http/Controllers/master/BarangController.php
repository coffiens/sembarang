<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BarangController extends Controller
{
    public function barang()
    {
        return view('master.barang.grid');
    }
    public function form()
    {
    	return view('master.barang.form');
    }
    public function edit()
    {
    	return view('master.barang.edit_barang');
    }
}
