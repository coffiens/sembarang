<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
  public function User()
    {
        return view('master.user.grid');
    }
    public function form()
    {
    	return view('master.user.form');
    }
    public function edit()
    {
    	return view('master.user.edit_user');
    }
}
